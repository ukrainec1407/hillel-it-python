

""" 2.Є два довільних числа які відповідають за мінімальну і максимальну ціну.
      Є Dict з назвами магазинів і цінами:
      {"cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324, "x-store": 37.166, "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}.
  Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких попадають в діапазон між мінімальною і максимальною ціною.
  Наприклад:
 lower_limit = 35.9
 upper_limit = 37.339
 > match: "x-store", "main-service" """

#Created dictionary:
shops = {
    "cito": 47.999,
    "BB_studio": 42.999,
    "momo": 49.999,
    "main-service": 37.245,
    "buy.now": 38.324,
    "x-store": 37.166,
    "the_partner": 38.988,
    "store": 37.720,
    "rozetka": 38.003
}


print("----------------------------------------------Solution with defined range----------------------------------------------------")


#min & max defined range

lower_limit = 35.9
upper_limit = 37.339

#selecting shops with their prices which come within a given range

for shop_price in shops.items():
    if shop_price[1] > lower_limit and shop_price[1] < upper_limit:
        print(shop_price)


print("----------------------------------------------Solution with the range taken from a user----------------------------------------------------")

#go through the maximum and minimum prices in the list of shops
max_value = max(shops.values())
min_value = min(shops.values())
#tell the user what these numbers are to give an idea of the possible limits of the minimum and maximum price
print(f"The maximum value of the price among the listed shops is {max_value}")
print(f"The minimum value of the price among the listed shops is {min_value}")

#min & max defining range

lower_limit = input("Give me your desired minimum price: ")

while True:
    if lower_limit.replace('.', '', 1).isdigit():
        lower_limit = float(lower_limit)
        if lower_limit >= min_value:
            break
        else:
            lower_limit = input("Give me the price that is more than minimum value of the price in the listed shops: ")
    else:
        lower_limit = input("Give me your desired minimum price that is digit: ")

upper_limit = input("Give me your desired maximum price: ")

while True:
    if upper_limit.replace('.', '', 1).isdigit():
        upper_limit = float(upper_limit)
        if upper_limit > lower_limit:
            if upper_limit <= max_value:
                break
            else:
                upper_limit = input("Give me your desired maximum price that less than maximum value of the price in the listed shops: ")
        else:
            upper_limit = input("Give me your desired maximum price that is more than minimum price: ")

    else:
        upper_limit = input("Give me your desired maximum price that is digit: ")

#selecting shops with their prices which come within a given range

for shop_price in shops.items():
    if shop_price[1] > lower_limit and shop_price[1] < upper_limit:
        print(shop_price)



