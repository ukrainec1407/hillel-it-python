"""Напишіть программу "Касир в кінотеватрі", яка буде виконувати наступне:
Попросіть користувача ввести свсвій вік (можно використати константу або input()).
- якщо користувачу менше 7 - вивести повідомлення"Де твої батьки?"
- якщо користувачу менше 16 - вивести повідомлення "Це фільм для дорослих!"
- якщо користувачу більше 65 - вивести повідомлення"Покажіть пенсійне посвідчення!"
- якщо вік користувача містить цифру 7 - вивести повідомлення "Вам сьогодні пощастить!"
- у будь-якому іншому випадку - вивести повідомлення "А білетів вже немає!"
P.S.На екран має бути виведено лише одне повідомлення, якщо вік користувача містить цифру 7 то має бути виведено тільки відповідне повідомлення!
Також подумайте над варіантами, коли введені невірні дані."""


age = input("Який Ваш вік? ")

if age.isdigit() and int(age) > 0:                         #перевіряємо чи ввів користувач число
    print(f'Зрозуміло! {age} це ваш вік')

    if age.find('7') != -1:               #якщо виконується дана умова(строка має 7, тобто індекс 7 більше -1),то повідомляємо про це
        print('Вам сьогодні пощастить!')
    else:                                 #якщо виконується дана умова(строка не має 7, тобто індекс 7 дорівнює -1),то повідомляємо про це
        age = int(age)                    #перетворюємо string на int

        if age < 7:
            print("Де твої батьки?")
        elif age < 16 and age > 7:
            print("Це фільм для дорослих!")
        elif age > 65:
            print("Покажіть пенсійне посвідчення!")
        else:
            print('А білетів вже немає!')
else:
    print('Потрібно ввести свій вік цифрами, та тобі має бути більше, ніж 0 років :/') #якщо користувач ввів не цифри,то повідомляємо про це




